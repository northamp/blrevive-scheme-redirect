# BLRE Redirector

Dead simple static web page that redirects people to `blredit://connect-server/` links depending on the `server` parameter passed to the request.

This is necessary due to the fact Discord doesn't like URLs that do not use `http` or `https` schemes.

Current deployment(s) can be found in the project's Gitlab environment(s).

Fun fact: page was mostly written [using ChatGPT](https://chat.openai.com/share/1094c118-87f1-4a1d-a2ef-3660af5ccbd6) (never used it before).
